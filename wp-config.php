<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'crystals_dev');

/** MySQL database username */
define('DB_USER', 'crystals_admin');

/** MySQL database password */
define('DB_PASSWORD', '800800crystalight');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'H:|lJWi*agYILsMNcP3Q~VS%M:; fbQu1_%hj_zpJ7zP(Gw<{%WM0<.XTGl]pS+5');
define('SECURE_AUTH_KEY',  'wWMgdht#}Yqktl~eHKIiknC`!!lx<s`VE`r)|)qSEz?(u=&$U/)1xfI:%Y0QX9o`');
define('LOGGED_IN_KEY',    'g=oME4qvG1LYLQ*`cd3DUy-4iusVD*{~<> i35P#hie+5vYZ$k%WrQTKI JppMOY');
define('NONCE_KEY',        'p<LrDG.Ahu9>y(@{LO?6;T(6xZh_X,6<#%FL{v{k*cynw-ye>B6LjwkoFQHW.l`S');
define('AUTH_SALT',        'u@NcfxLsU5.euLG.BR#tm.~OvVXraHNN@?K{,OX)ut^KK;ZBs#K@xV=40m0bTwlC');
define('SECURE_AUTH_SALT', '{E+vy[fPA-bqxfXQH^s>OHEw- sF5qh29u2-$|e>,iXGl;Xt!q]lB*d8/J1h31Ga');
define('LOGGED_IN_SALT',   'TXI4*1KZ>x=R:QBBn}$DBBnsr;*tHty-3VKfwrnrDSR5JS@@^gpH:^gFOtNrXzYs');
define('NONCE_SALT',       '}(;GV2!.tq=h4phZbNM`4o(7.azs!;%ZqDRw.&QeV;yUDp6IklqC:-8z|qWJRTq_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_cylight_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
