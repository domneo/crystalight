<?php get_header(); ?>

	<main role="main" class="nosidebar">
		<!-- section -->
		<section>

			<!-- article -->
			<article id="post-404">

				
				<div class="fa-stack fa-lg">
					<i class="fa fa-calendar-o fa-stack-2x"></i>
					<i class="fa fa-question fa-stack-1x"></i>
				</div>

				<div class="text-404">
					<h1><?php _e( 'Page not found', 'html5blank' ); ?></h1>
					<h2>
						<a href="<?php echo home_url(); ?>/product-category/calendars-diaries/">Browse Calendars & Diaries <i class="fa fa-arrow-right"></i></a>
					</h2>
					<h2>
						<a href="<?php echo home_url(); ?>/product-category/corporate-gifts/">Browse Corporate Gifts <i class="fa fa-arrow-right"></i></a>
					</h2>
				</div>

			</article>
			<!-- /article -->

		</section>
		<!-- /section -->
	
	</main>

<?php get_footer(); ?>
