<?php /* Template Name: Logo Printing Options Template */ get_header(); ?>

	<main role="main" class="logoprint nosidebar noheader">
		<!-- section -->
		<section>

			<div id="logo-imprints-text-box">
				<h1><strong>Logo Printing Options for Different Products</strong></h1>
				<p>Below are examples of logo printing options for products like corporate diaries, organisers, portfolios, planners, note books and corporate gifts like electronic gadgets, pens, mugs, etc. Kindly contact our sales team regarding the type of logo printing option suitable for your selected choice.</p>
			</div>
			<div id="logo-imprints-right">
				<div>
					<div class="logo-imprints-pair">
						<div class="logo-imprints-item">
							<img alt="Gold/Silver Hot-Stamping on Foamsheet PVC" src="<?php echo get_template_directory_uri(); ?>/img/logo-imprints/gold-silver-hotstamp1.jpg" width="125" height="auto" />
							<p>Gold/Silver<br />Hot-Stamping on Foamsheet PVC</p>
						</div>
						<div class="logo-imprints-item">
							<img alt="Gold/Silver Silkscreen Printing on PU Cover" src="<?php echo get_template_directory_uri(); ?>/img/logo-imprints/gold-silver-silkscreen.jpg" width="125" height="auto" />
							<p>Gold/Silver Silkscreen Printing on PU Cover</p>
						</div>
					</div>
					<div class="logo-imprints-pair">
						<div class="logo-imprints-item">
							<img alt="Blind Debossing on PU Cover" src="<?php echo get_template_directory_uri(); ?>/img/logo-imprints/blind-emboss.jpg" width="125" height="auto" />
							<p>Blind Debossing<br />on PU Cover</p>
						</div>
						<div class="logo-imprints-item">
							<img alt="Gold/Silver Debossed Hot-stamping on PU Cover" src="<?php echo get_template_directory_uri(); ?>/img/logo-imprints/gold-silver-emboss-hotstamp.jpg" width="125" height="auto" />
							<p>Gold/Silver Debossed<br />Hot-Stamping<br />on PU Cover</p>
						</div>
					</div>
				</div>
				<div style="clear: both;">
					<div class="logo-imprints-pair">
						<div class="logo-imprints-item">
							<img alt="2 Colour Silk-Screen on Products" src="<?php echo get_template_directory_uri(); ?>/img/logo-imprints/2-colour-silkscreen.jpg" width="125" height="auto" />
							<p>2 Colour Silk-Screen on Products</p>
						</div>
						<div class="logo-imprints-item">
							<img alt="1 Colour Silk-Screen on Pens" src="<?php echo get_template_directory_uri(); ?>/img/logo-imprints/1-colour-silkscreen.jpg" width="125" height="auto" />
							<p>1 Colour Silk-Screen on Pens</p>
						</div>
					</div>
					<div class="logo-imprints-pair">
						<div class="logo-imprints-item">
							<img alt="Laser Engraving on Metal Surfaces" src="<?php echo get_template_directory_uri(); ?>/img/logo-imprints/laser-engraving.jpg" width="125" height="auto" />
							<p>Laser Engraving<br />on Metal Surfaces</p>
						</div>
						<div class="logo-imprints-item">
							<img alt="1 Colour Silk-Screen on Mugs" src="<?php echo get_template_directory_uri(); ?>/img/logo-imprints/1-colour-silkscreen-mugs.jpg" width="125" height="auto" />
							<p>1 Colour Silk-Screen on Mugs</p>
						</div>
					</div>
				</div>
			</div>

		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
