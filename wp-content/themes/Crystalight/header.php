<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<meta name="Keywords" content="advertising promotional gifts, corporate gifts, door gifts, custom-made products, product launch, opening gift, dinner and dance, family day, wines sets, desktop gifts, bags, sports bags, shoe bags, document bags, mug, cap, t-shirt, windbreaker, jacket, pen, watches, high quality leather products, premium gifts, promotion items, stockist, calendar manufacturer, diary manufacturer, premium gift industry, corporate promotions, corporate functions, bags, cap, T-shirit, desktop stationary, office stationary, electronic goods, fancy gifts, gift set, golf related, 
household products, IT - related products, leather goods, portfolio organiser, sports, leisure, clocks, wine accessories, writing instrument" />

		<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

		<?php wp_head(); ?>

		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

        <meta name="google-site-verification" content="NgRcXzs3yZrSU3r1FpXeXe1BQJr_-GADUrMqn81szpM" />

        <script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-57954999-1', 'auto');
		  ga('send', 'pageview');

		</script>
        
	</head>
	<body <?php body_class(); ?>>

		<!-- sb-site -->
		<div id="sb-site">

			<!-- header -->
			<header class="header clear" role="banner">

				<div class="logo-wrapper">

				    <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-area-4')) ?>
					
					<!-- logo -->
						<a href="<?php echo home_url(); ?>" class="logo-img">
							<!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="Crystalight Printcraft">
						</a>
					<!-- /logo -->

                    <img src="<?php echo get_template_directory_uri(); ?>/img/header-img.png" class="header-img">

				</div>
			
			</header>
			<!-- /header -->

			<!-- nav -->
			<nav class="nav" role="navigation">

				<div class="nav-wrapper">
				
					<?php html5blank_nav(); ?>

					<!-- hotline -->
					<div class="hotline">
						<i class="fa fa-phone"></i>Hotline: +65 6251 5313
					</div>
					<!-- /hotline -->

				</div>

			</nav>
			<!-- /nav -->

			<div class="menu-bar">
				<div class="menu-button sb-toggle-left"><i class="fa fa-navicon"></i>Menu</div>

				<!-- hotline -->
				<div class="hotline-2">
					<i class="fa fa-phone"></i>Hotline: +65 6251 5313
				</div>
				<!-- /hotline -->

				<div class="products-button sb-toggle-right">Products<i class="fa fa-list-ul"></i></div>
				
			</div>

			<!-- wrapper -->
			<div class="wrapper">