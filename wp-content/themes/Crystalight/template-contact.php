<?php /* Template Name: Contact Us Page Template */ get_header(); ?>

	<main role="main" class="contact">
		
		<div style="max-width:1280px;margin:0 auto;">

			<section>

				<h1><?php the_title(); ?></h1>

				<hr style="border:0; width:100%; background-color:#CCC; color:#CCC; height:1px; margin:0;" />

				<div id="company-name" style="float: left;">
					<h2 style="text-transform:uppercase;"><strong><?php echo get_bloginfo('name'); ?></strong></h2>
				</div>

				<div id="reg-numbers">
					<p style="text-align: right;">
						<?php $contactpage_id = 'contact'; ?>

						<?php $regnums = types_render_field( 'reg-num', array( 'id' => $contactpage_id, 'separator' => '</strong><br><strong class="label-grey">' ) ); ?>

						<?php if (!empty( $regnums )) { ?>
							<strong class="label-grey"><?php echo $regnums; ?></strong>
						<?php } ?>
					</p>
				</div>

				<hr style="border:0; width:100%; background-color:#CCC; color:#CCC; height:1px; margin:0;" />

				<div id="contact-details">

					<?php
					$contact_address = types_render_field( 'contact-address', array( 'id' => $contactpage_id ) );
					$contact_gmaps = types_render_field( 'contact-gmaps', array( 'id' => $contactpage_id, 'output' => 'raw' ) );
					$contact_tel = types_render_field( 'contact-tel', array( 'id' => $contactpage_id ) );
					$contact_fax = types_render_field( 'contact-fax', array( 'id' => $contactpage_id ) );
					$contact_email = types_render_field( 'contact-email', array( 'id' => $contactpage_id ) );
					?>

					<?php if (!empty( $contact_address )) { ?>
						<a href="<?php echo $contact_gmaps; ?>" target="_blank">
							<i class="fa fa-location-arrow fa-2x"></i><span>Address: <strong><?php echo $contact_address; ?></strong></span>
						</a>
					<?php } ?>
					<?php if (!empty( $contact_tel )) { ?>
						<a href="tel:<?php echo $contact_tel; ?>">
							<i class="fa fa-phone fa-2x"></i><span>Telephone: <strong><?php echo $contact_tel; ?></strong></span>
						</a>
					<?php } ?>
					<?php if (!empty( $contact_fax )) { ?>
						<a href="tel:<?php echo $contact_fax; ?>">
							<i class="fa fa-fax fa-2x"></i><span>Fax: <strong><?php echo $contact_fax; ?></strong></span>
						</a>
					<?php } ?>
					<?php if (!empty( $contact_email )) { ?>
						<a href="mailto:<?php echo $contact_email; ?>">
							<i class="fa fa-envelope fa-2x"></i><span>Email: <strong><?php echo $contact_email; ?></strong></span>
						</a>
					<?php } ?>

				</div>

			</section>

			<aside class="sidebar sidebar-right google-maps-widget" role="complementary">
				
				<?php
				$location_title = types_render_field( 'location-title', array( 'id' => $contactpage_id ) );
				$location_directions = types_render_field( 'location-directions', array( 'id' => $contactpage_id ) );
				$location_gmaps_embed = types_render_field( 'location-gmaps-embed', array( 'id' => $contactpage_id ) );
				?>
				
				<?php if (!empty( $location_title )) { ?>
					<h3><?php echo $location_title; ?></h3>
				<?php } ?>
				<?php if (!empty( $location_directions )) { ?>
					<p><?php echo $location_directions; ?></p>
				<?php } ?>
				<?php if (!empty( $location_gmaps_embed )) { ?>
					<?php echo $location_gmaps_embed; ?>
				<?php } ?>
				
			</aside>

		</div>

	</main>

<?php get_footer(); ?>
