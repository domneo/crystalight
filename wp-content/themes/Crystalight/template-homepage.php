<?php /* Template Name: Homepage Template */ get_header(); ?>

<main role="main" class="nosidebar noheader">
    <!-- section -->
    <section>

        <div class="homeslider-wrapper">
            <ul class="homeslider">

            <?php $slides = new WP_Query( array( 'post_type' => 'homepage-slide' ) ); ?>
            <?php if ( $slides->have_posts() ) : ?>
            <?php while ( $slides->have_posts() ) : $slides->the_post(); ?>

                <?php
                $slide_img = types_render_field( 'slide-image', array('url' => 'true') );
                $slide_alttext = types_render_field( 'slide-image-alt-text' );
                $slide_caption = types_render_field( 'slide-caption' );
                $slide_url = types_render_field( 'slide-url', array('output' => 'raw') );
                ?>

                <li>
                    <a <?php if (!empty( $slide_url )) { ?>href="<?php echo $slide_url ?>" <?php } ?>>
                        <?php if (!empty( $slide_img )) { ?>
                            <img src="<?php echo $slide_img ?>" alt="<?php echo $slide_alttext ?>" />
                        <?php } ?>
                    </a>
                    <?php if (!empty( $slide_caption )) { ?>
                        <h1><?php echo $slide_caption ?></h1>
                    <?php } ?>
                </li>

            <?php endwhile; ?>
            <?php endif; ?>

            </ul>
        </div>

        <div class="featured-items">

            <?php
            $frontpage_id = get_option( 'page_on_front' );

            $featured_1_img = types_render_field( 'featured-item-1-img', array('url' => 'true', 'id' => $frontpage_id) );
            $featured_1_title = types_render_field( 'featured-item-1-title', array('id' => $frontpage_id) );
            $featured_1_desc = types_render_field( 'featured-item-1-desc', array('id' => $frontpage_id) );
            $featured_1_btntext = types_render_field( 'featured-item-1-btn', array('id' => $frontpage_id) );
            $featured_1_url = types_render_field( 'featured-item-1-url', array('output' => 'raw', 'id' => $frontpage_id) );
            
            $featured_2_img = types_render_field( 'featured-item-2-img', array('url' => 'true', 'id' => $frontpage_id) );
            $featured_2_title = types_render_field( 'featured-item-2-title', array('id' => $frontpage_id) );
            $featured_2_desc = types_render_field( 'featured-item-2-desc', array('id' => $frontpage_id) );
            $featured_2_btntext = types_render_field( 'featured-item-2-btn', array('id' => $frontpage_id) );
            $featured_2_url = types_render_field( 'featured-item-2-url', array('output' => 'raw', 'id' => $frontpage_id) );

            $featured_3_img = types_render_field( 'featured-item-3-img', array('url' => 'true', 'id' => $frontpage_id) );
            $featured_3_title = types_render_field( 'featured-item-3-title', array('id' => $frontpage_id) );
            $featured_3_desc = types_render_field( 'featured-item-3-desc', array('id' => $frontpage_id) );
            $featured_3_btntext = types_render_field( 'featured-item-3-btn', array('id' => $frontpage_id) );
            $featured_3_url = types_render_field( 'featured-item-3-url', array('output' => 'raw', 'id' => $frontpage_id) );
            ?>

            <div class="feature">
                <a <?php if (!empty( $featured_1_url )) { ?>href="<?php echo $featured_1_url ?>" <?php } ?>>
                    <?php if (!empty( $featured_1_img )) { ?>
                        <img src="<?php echo $featured_1_img ?>" alt="<?php echo $featured_1_title ?>">
                    <?php } ?>
                </a>
                <div class="desc">
                    <?php if (!empty( $featured_1_title )) { ?>
                        <h2><?php echo $featured_1_title ?></h2>
                    <?php } ?>
                    <?php if (!empty( $featured_1_desc )) { ?>
                        <p><?php echo $featured_1_desc ?></p>
                    <?php } ?>
                    <?php if (!empty( $featured_1_btntext )) { ?>
                        <a href="<?php echo $featured_1_url ?>" class="btn-grey"><?php echo $featured_1_btntext ?></a>
                    <?php } ?>
                </div>
            </div>

            <div class="feature">
                <a <?php if (!empty( $featured_2_url )) { ?>href="<?php echo $featured_2_url ?>" <?php } ?>>
                    <?php if (!empty( $featured_2_img )) { ?>
                        <img src="<?php echo $featured_2_img ?>" alt="<?php echo $featured_2_title ?>">
                    <?php } ?>
                </a>
                <div class="desc">
                    <?php if (!empty( $featured_2_title )) { ?>
                        <h2><?php echo $featured_2_title ?></h2>
                    <?php } ?>
                    <?php if (!empty( $featured_2_desc )) { ?>
                        <p><?php echo $featured_2_desc ?></p>
                    <?php } ?>
                    <?php if (!empty( $featured_2_btntext )) { ?>
                        <a href="<?php echo $featured_2_url ?>" class="btn-grey"><?php echo $featured_2_btntext ?></a>
                    <?php } ?>
                </div>
            </div>

            <div class="feature">
                <a <?php if (!empty( $featured_3_url )) { ?>href="<?php echo $featured_3_url ?>" <?php } ?>>
                    <?php if (!empty( $featured_3_img )) { ?>
                        <img src="<?php echo $featured_3_img ?>" alt="<?php echo $featured_3_title ?>">
                    <?php } ?>
                </a>
                <div class="desc">
                    <?php if (!empty( $featured_3_title )) { ?>
                        <h2><?php echo $featured_3_title ?></h2>
                    <?php } ?>
                    <?php if (!empty( $featured_3_desc )) { ?>
                        <p><?php echo $featured_3_desc ?></p>
                    <?php } ?>
                    <?php if (!empty( $featured_3_btntext )) { ?>
                        <a href="<?php echo $featured_3_url ?>" class="btn-grey"><?php echo $featured_3_btntext ?></a>
                    <?php } ?>
                </div>
            </div>
            
        </div>

        <br class="clear">

    </section>
    <!-- /section -->
</main>

<?php get_footer(); ?>