				<!-- footer -->
				<footer class="footer" role="contentinfo">
					<div>
						<div class="footer-left">

							<?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>

							<!-- copyright -->
							<p class="copyright">
								&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. All Rights Reserved.
							</p>
							<!-- /copyright -->

						</div>

						<div class="footer-right">
							<p>Blk 13 Lor 8 Toa Payoh #05-08 Braddell Tech Singapore 319261<br>
							Tel: <a href="tel:+65 6251 5313">+65 6251 5313</a> • Fax: <a href="tel:+65 6254 5313">+65 6254 5313</a> • Email: <a href="mailto:enquiry@crystalight.com.sg">enquiry@crystalight.com.sg</a></p>
						</div>
					</div>
				</footer>
				<!-- /footer -->

			</div>
			<!-- /wrapper -->

		</div>
		<!-- /sb-site -->

		<div class="sb-slidebar sb-left">
			<!-- nav -->
			<nav class="nav" role="navigation">

				<div class="nav-wrapper">

					<div class="close-button sb-toggle-left">
						close <i class="fa fa-times"></i>
					</div>

					<?php wp_nav_menu( array( 'theme_location' => 'sidebar-menu' ) ); ?>

					<!-- hotline -->
					<div class="hotline">
						<a href="tel:+65 6251 5313"><i class="fa fa-phone"></i>Hotline: +65 6251 5313</a>
					</div>
					<!-- /hotline -->

				</div>

			</nav>
			<!-- /nav -->
		</div>

		<div class="sb-slidebar sb-right">
			<?php get_sidebar('products'); ?>
		</div>

		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>

		<!-- Slidebars -->
		<script src="<?php echo get_template_directory_uri(); ?>/js/slidebars.js"></script>
		<script>
		  (function($) {
		    $(document).ready(function() {
		      $.slidebars({
		        siteClose: true, // true or false - Enable closing of Slidebars by clicking on #sb-site.
				scrollLock: true, // true or false - Prevent scrolling of site when a Slidebar is open.
				disableOver: 768, // integer or false - Hide Slidebars over a specific width.
				hideControlClasses: true // true or false - Hide controls at same width as disableOver.
		      });
		    });
		  }) (jQuery);
		</script>

	</body>
</html>
