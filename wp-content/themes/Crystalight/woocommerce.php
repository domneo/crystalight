<?php get_header(); ?>

	<main role="main">

		<div style="max-width:1280px;margin:0 auto;">

			<?php get_sidebar(); ?>

			<!-- section -->
			<section>
				
				<?php woocommerce_content(); ?>

			</section>
			<!-- /section -->

		</div>
		
	</main>

<?php get_footer(); ?>
