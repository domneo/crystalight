<?php get_header(); ?>

	<main role="main" class="default">

		<div style="max-width:1280px;margin:0 auto;">

			<?php get_sidebar(); ?>

			<!-- section -->
			<section>

				<h1><?php the_title(); ?></h1>

			<?php if (have_posts()): while (have_posts()) : the_post(); ?>

				<!-- div -->
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<?php the_content(); ?>

					<br class="clear">

				</div>
				<!-- /div -->

			<?php endwhile; ?>

			<?php else: ?>

				<!-- div -->
				<div>

					<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

				</div>
				<!-- /div -->

			<?php endif; ?>

			</section>
			<!-- /section -->

		</div>

	</main>

<?php get_footer(); ?>
