<?php /* Template Name: Disclaimer Template */ get_header(); ?>

	<main role="main" class="nosidebar">
		<!-- section -->
		<section>

			<h1><?php the_title(); ?></h1>

			<strong>Copyright <?php echo date('Y'); ?> &copy; Crystalight Printcraft, Singapore.</strong>
			
			<div class="disclaimertext">
				<strong>Terms and conditions</strong>
				<p>Please read all these terms and conditions contained in this page carefully. By accessing this site and any of the pages thereof, you are agreeing to be bound by the terms and conditions below. If you are not agreeable to the terms and conditions set out below, do not access the site, or any of pages thereof.</p>
				<p>Copyright <?php echo date('Y'); ?> &copy; Crystalight Printcraft. All rights reserved. Copyright in Crystalight Printcraft Web Sites, and in the information and materials therein and in their arrangement, are owned by Crystalight Printcraft unless otherwise indicated.</p>
			</div>

			<div class="disclaimertext">
				<strong>Use of information and materials</strong>
				<p>The information and materials contained in these pages - and the terms, conditions, and descriptions that appear - are subject to change at the discretion of Crystalight Printcraft.</p>
			</div>

			<div class="disclaimertext">
				<strong>Liability Disclaimers</strong>
				<p>The information and materials contained in this site, including text, graphics, links or other items - are provided “as is”, “as available”. Crystalight Printcraft does not warrant the accuracy, adequacy or completeness of this information and materials and expressly disclaims liability for errors or omissions in this information and materials.</p>
				<p>No warranty of any kind, implied, expressed or statutory, including but not limited to the warranties of non-infringement of the third party rights, title, merchantability, fitness for a particular purpose and freedom from computer virus, is given in conjunction with the information and materials.</p>
				<p>The Crystalight Printcraft website may from time to time feature information and content that may be offensive. Whilst all measures are taken to ensure that the material remains within the realms of good taste, Crystalight Printcraft do not accept any liability for any offence caused by any text or images within these pages.</p>
				<p>Logos or Trademarks being used in the Crystalight Printcraft Web Sites to show the various nature of imprint on products do not represent an endorsement or product order by the respective owners.</p>
				<p>This Web site may include publications with technical inaccuracies or typographical errors that will be corrected as they are discovered at the Crystalight Printcraft’s sole discretion. Also, changes are periodically added to the information contained herein. These corrections and changes may be incorporated into the Crystalight Printcraft Web Sites at a later date. Crystalight Printcraft may at any time make modifications, improvements and/or changes to these Terms and Conditions, the information, names, images, pictures, logo and icons displayed on the Crystalight Printcraft Web Sites or products and services referred to within, with or without notice.</p>
				<p>Crystalight Printcraft Web Sites may contain links to or references to other web sites that may be accessed may originate outside the boundaries of the Crystalight Printcraft Web Sites and which information Crystalight Printcraft will have no control. Therefore Crystalight Printcraft shall have no obligation or responsibility regarding any content derived, obtained, accessed within, through or outside Crystalight Printcraft Web Sites.</p>
				<p>In no event shall Crystalight Printcraft, or any of its subsidiaries, affiliates, marketing partners be liable for any damages, including without limitation direct or indirect, special, incidental, or consequential damages, or any damages, whatsoever resulting from access or use, or inability to access or use the Crystalight Printcraft Web Sites or arising out of any materials, information, qualifications or recommendations on this Web site.</p>
			</div>

			<br class="clear">

		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
