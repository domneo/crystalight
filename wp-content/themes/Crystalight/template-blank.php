<?php /* Template Name: Blank Page */ ?>

<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<meta name="Keywords" content="advertising promotional gifts, corporate gifts, door gifts, custom-made products, product launch, opening gift, dinner and dance, family day, wines sets, desktop gifts, bags, sports bags, shoe bags, document bags, mug, cap, t-shirt, windbreaker, jacket, pen, watches, high quality leather products, premium gifts, promotion items, stockist, calendar manufacturer, diary manufacturer, premium gift industry, corporate promotions, corporate functions, bags, cap, T-shirit, desktop stationary, office stationary, electronic goods, fancy gifts, gift set, golf related, 
household products, IT - related products, leather goods, portfolio organiser, sports, leisure, clocks, wine accessories, writing instrument" />

		<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

        <meta name="google-site-verification" content="NgRcXzs3yZrSU3r1FpXeXe1BQJr_-GADUrMqn81szpM" />

        <script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-57954999-1', 'auto');
		  ga('send', 'pageview');

		</script>
        
	</head>
	<body <?php body_class(); ?> style="margin:0;">

	<main role="main" class="blankpage" style="position:absolute; width:100%; height:100%;">
		<!-- section -->
		<section style="height:100%;">

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- div -->
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="height:100%;">

				<?php the_content(); ?>

				<br class="clear">

			</div>
			<!-- /div -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- div -->
			<div>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</div>
			<!-- /div -->

		<?php endif; ?>

		</section>
		<!-- /section -->
	</main>