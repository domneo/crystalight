<?php /* Template Name: About Page Template */ get_header(); ?>

	<main role="main" class="about nosidebar noheader">
		<!-- section -->
		<section>

			<div style="max-width: 900px; margin: 0 auto; display: table; line-height: 2.5em;">
				<div class="aboutslider-wrapper-1">
					<ul class="aboutslider-1">
						<?php
						$big_slides = types_render_field( 'big-img-slides', array('id' => 'about') );
						printf($big_slides);
						?>
					</ul>
				</div>
				<div class="aboutslider-right">
					<div class="aboutslider-wrapper-2">
						<ul class="aboutslider-2">
							<?php
							$small_slides_1 = types_render_field( 'small-img-slides-1', array('id' => 'about') );
							printf($small_slides_1);
							?>
						</ul>
						<ul class="aboutslider-3">
							<?php
							$small_slides_2 = types_render_field( 'small-img-slides-2', array('id' => 'about') );
							printf($small_slides_2);
							?>
						</ul>
						<ul class="aboutslider-4">
							<?php
							$small_slides_3 = types_render_field( 'small-img-slides-3', array('id' => 'about') );
							printf($small_slides_3);
							?>
						</ul>
						<ul class="aboutslider-5">
							<?php
							$small_slides_4 = types_render_field( 'small-img-slides-4', array('id' => 'about') );
							printf($small_slides_4);
							?>
						</ul>
					</div>
					<div style="width: 90%; margin: 5% auto 0;">
						<h1><?php the_title(); ?></h1>
						<?php if (have_posts()): while (have_posts()) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>

		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
