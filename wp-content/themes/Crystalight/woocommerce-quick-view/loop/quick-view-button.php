<?php
/**
 * Quick View Button
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

echo apply_filters( 'woocommerce_loop_quick_view_button', sprintf( '<div class="qv-btn"><a href="%s" title="%s" class="quick-view-button button"><i class="fa fa-eye"></i>%s</a></div>', esc_url( $link ), esc_attr( get_the_title() ), __( 'Quick View', 'wc_quick_view' ) ) );